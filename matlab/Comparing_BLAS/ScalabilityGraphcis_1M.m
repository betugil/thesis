%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot the scability of the IBM Blue Gene / Q system for the hp2FEM
% software

clear
close all
clc


format long

BaseFile=pwd;
BaseFile=strcat(BaseFile,'/figures/eps/');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Plot Scalability - P = 1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Number of Ranks, Polynomial Order, Number of DOFs, The elapsed time
%%ParallelSolver_StrongScaling_1M = [
%%0.0009765625     1    123427.9831                                   ;
%%0.001953125      2                                                  ;
%%0.00390625       4                                                  ;
%%0.0078125        8                                                  ;
%%0.015625        16                                                  ;
ParallelSolver_StrongScaling_1M = [
0.03125         32      3987.104065       30.9568       0.9674      ;
0.0625          64      1961.878801       62.91315399   0.9830180311;
0.125          128       981.840217      125.7108652    0.982116134 ;
0.25           256       491.554016      251.0974971    0.9808495981;
0.5            512       247.857412      497.9797946    0.9726167863;
1             1024       124.512954      991.2862811    0.9680530089;
2             2048        62.719494     1967.936526     0.9609065067;
4             4096        31.795772     3881.899239     0.9477293063;
8             8192        16.27863      7582.209505     0.9255626838;
16           16384         8.534283    14462.60724      0.8827274928;
32           32768         4.668935    26436.00374      0.8067628094
];



%% PLOT - SCALABILITY
%CREATEFIGURE(X1, YMATRIX1)
%  X1:  vector of x data
%  YMATRIX1:  matrix of y data

%  Auto-generated by MATLAB on 18-Jun-2015 11:03:25

% Create figure
% 
% figure % create new figure
SpeedUp_Ideal = ParallelSolver_StrongScaling_1M(:,2);
SpeedUp = ParallelSolver_StrongScaling_1M(:,4);
Efficiency = ParallelSolver_StrongScaling_1M(:,5);

X1=ParallelSolver_StrongScaling_1M(:,2);
X2=[0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1]';
%YMatrix1=[SpeedUp_Version1_P1, SpeedUp_NewWithBlasIBM_P1, SpeedUp_NewWithBlasNetlib_P1, SpeedUp_Ideal];
%Legend = {'Speed Up (First Version)'; 'Speed Up (BLAS IBM)'; 'Speed Up (BLAS Netlib)'; 'Speed Up (Ideal)'};
YMatrix1=[SpeedUp];
YMatrix2=[Efficiency];
Legend = {'Speed Up'};
title_str={''};
xlabel_str='Number of Procesors';
ylabel_str='SpeedUp (Ts/Tp)';
figure_file=strcat(BaseFile,'Scalability_Hexa_10000_P1.eps');
% 
% fig=plotyy(X1, YMatrix1, X1, YMatrix2);
% set(fig,'yscale','log','xscale','log');
% set(fig(1),'ylim',[1 33000]);
% set(fig(2),'ylim',[0 1.1]);
% ax.XTick = [32 64 128 256 512 1024 2048 4096 8192 16384 32768];
% ax.YTick = [32 64 128 256 512 1024 2048 4096 8192 16384 32768];
% set(fig(2),'ylim',[1 33000]);
plotscalability_allmira(X1,YMatrix1,YMatrix2);
% plot(X2, YMatrix2);
% plotscalability(X1, YMatrix1, title_str, Legend, xlabel_str, ylabel_str, figure_file);


