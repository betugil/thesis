#!/bin/bash

MESH=Hexa10000_11466_BLAS_IBM
#MESH=Hexa10000_11466_BLAS_Netlib
#MESH=Hexa10000_11466_D1Matrices
POLY_ORDER=(1 3 6 9);
FILE=$MESH"_Informations.m"

echo -e "Number of Ranks, Polynomial Order, Number of DOFs, The elapsed time\n" > $FILE

ind=0;
pi_start=0;
pi_end=4;

for (( ind=$pi_start; ind<pi_end; ind++ )); do
	# echo ${POLY_ORDER[ind]}

	NumRanksTmp=$(grep -nR ProjectionSolver_Parallel $MESH"_P"${POLY_ORDER[ind]}".csv" | gawk -F 'NumRanks = ' '{print $2}');
	NumDofsTmp=$(grep -nR ProjectionSolver_Parallel $MESH"_P"${POLY_ORDER[ind]}".csv" | gawk -F 'Total Number of Dofs : ' '{print $2}');
	TimeTmp=$(grep -nR ProjectionSolver_Parallel $MESH"_P"${POLY_ORDER[ind]}".csv"  | gawk -F 'The elapsed time = ' '{print $2}' | cut -d, -f1);

	NumRanksTmp="1 "$NumRanksTmp;
	NumDofsTmp=$(echo $NumDofsTmp | cut -d' ' -f1)$NumDofsTmp
	TimeTmp_Aux=$(grep -nR ProjectionSolver_Serial $MESH"_P"${POLY_ORDER[ind]}".csv"  | gawk -F 'The elapsed time =,' '{print $2}' | cut -d',' -f1 | cut -d' ' -f2);
	TimeTmp=$TimeTmp_Aux$TimeTmp
	# echo -e $NumRanksTmp
	# echo -e $TimeTmp_Aux

	NumRanks=($NumRanksTmp);
	NumDofs=($NumDofsTmp);
	Time=($TimeTmp);

	TAM=12;
	j=0;

	echo -e "ParallelSolver_Informations = [" >> $FILE;
	for i in $(seq $TAM);
	do
		echo -e ${NumRanks[j]}",\t "$POLY_ORDER",\t "${NumDofs[j]}",\t "${Time[j]};
		j=$((j+1));
	done >> $FILE;

	echo -e "]" >> $FILE;
	echo -e "\n" >> $FILE;
done;

##echo ${POLY_ORDER[ind]}
#POLY_ORDER=9
#NumRanksTmp=$(grep -nR ProjectionSolver_Parallel $MESH"_P"$POLY_ORDER".csv" | gawk -F 'NumRanks = ' '{print $2}');
#NumDofsTmp=$(grep -nR ProjectionSolver_Parallel $MESH"_P"$POLY_ORDER".csv" | gawk -F 'Total Number of Dofs : ' '{print $2}');
#TimeTmp=$(grep -nR ProjectionSolver_Parallel $MESH"_P"$POLY_ORDER".csv"  | gawk -F 'The elapsed time = ' '{print $2}' | cut -d, -f1);
#
#NumRanks=($NumRanksTmp);
#NumDofs=($NumDofsTmp);
#Time=($TimeTmp);
#
#TAM=11;
#j=0;
#
#echo -e "ParallelSolver_Informations = [" >> $FILE;
#for i in $(seq $TAM);
#do
#	echo ${NumRanks[j]}", "$POLY_ORDER", "${NumDofs[j]}", "${Time[j]};
#	j=$((j+1));
#done >> $FILE;
#
#echo -e "]" >> $FILE;
#echo -e "\n" >> $FILE;

