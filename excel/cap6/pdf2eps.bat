rem pdf2eps <page number> <pdf file without ext>
echo off
pdfcrop.exe "%2.pdf" "%2-temp.pdf"
pdftops.exe -f %1 -l %1 -eps "%2-temp.pdf" "%2.eps"
del  "%2-temp.pdf"
