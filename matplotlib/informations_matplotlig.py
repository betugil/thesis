#!/usr/bin/python3
import matplotlib.pyplot as plt

plt.rcParams['savefig.bbox']='standard'
plt.rcParams['axes.linewidth']=2
plt.rcParams['axes.grid']=True
plt.rcParams['grid.linestyle']='-'
plt.rcParams['grid.linewidth']=1
plt.rcParams["savefig.facecolor"]='white'
plt.rcParams['savefig.edgecolor']='black'
plt.rcParams['figure.edgecolor']='black'
plt.rcParams["figure.facecolor"]='white'
plt.rcParams["axes.facecolor"]='white'
plt.rcParams['axes.edgecolor']='black'
