#!/usr/bin/python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math
import os
from pathlib import Path
from matplotlib.pyplot import figure, draw



#########################################################################
#   Configurações globais do grafico
plt.rcParams['axes.linewidth']=2
plt.rc('font', size=18)         # controls default text sizes
#########################################################################

#########################################################################
#   Configurações para salvar e plotar o grafico
BASE_PATH = os.getcwd()
PATH_SVG=BASE_PATH+"/../../../../inkscape/cap6/"
PATH_EPS=BASE_PATH+"/../../../../eps/cap6/"
fname = "Projection_Square10000_ClockCounterWise_Vs_NotSequential"
fname_svg = PATH_SVG+fname+".svg"
fname_eps = PATH_EPS+fname+".eps"
PLOT_FIG = True
SAVE_FIG = False
#########################################################################



#   Leitura do csv para pandas
##### Resultado para CentralDifferenceLinear Local Method
# df = pd.read_csv('./data/cdl-linear_mpi-openmp.csv')
##### Resultado para CentralDifferenceLinear Local Method - Multiplicate Operations
df = pd.read_csv('./data/Projection_Square10000_ClockCounterWise_Vs_NotSequential.csv', sep=";")
pd.set_option('expand_frame_repr', False)

#   Calculate SpeedUp - ClockCounterWise
df["SpeedUp P1 - ClockCounterWise"] = df.iloc[0]["P1 - ClockCounterWise - Runtime (s)"]/df["P1 - ClockCounterWise - Runtime (s)"]
df["SpeedUp P4 - ClockCounterWise"] = df.iloc[0]["P4 - ClockCounterWise - Runtime (s)"]/df["P4 - ClockCounterWise - Runtime (s)"]
df["SpeedUp P6 - ClockCounterWise"] = df.iloc[0]["P6 - ClockCounterWise - Runtime (s)"]/df["P6 - ClockCounterWise - Runtime (s)"]
df["SpeedUp P9 - ClockCounterWise"] = df.iloc[0]["P9 - ClockCounterWise - Runtime (s)"]/df["P9 - ClockCounterWise - Runtime (s)"]

#   Calculate SpeedUp - NotSequential
df["SpeedUp P1 - NotSequential"] = df.iloc[0]["P1 - NotSequential - Runtime (s)"]/df["P1 - NotSequential - Runtime (s)"]
df["SpeedUp P4 - NotSequential"] = df.iloc[0]["P4 - NotSequential - Runtime (s)"]/df["P4 - NotSequential - Runtime (s)"]
df["SpeedUp P6 - NotSequential"] = df.iloc[0]["P6 - NotSequential - Runtime (s)"]/df["P6 - NotSequential - Runtime (s)"]
df["SpeedUp P9 - NotSequential"] = df.iloc[0]["P9 - NotSequential - Runtime (s)"]/df["P9 - NotSequential - Runtime (s)"]

#   Calculate Efficiency - ClockCounterWise
df["Efficiency P1 - ClockCounterWise"] = df["SpeedUp P1 - ClockCounterWise"]/df["Num. Nodes"]
df["Efficiency P4 - ClockCounterWise"] = df["SpeedUp P4 - ClockCounterWise"]/df["Num. Nodes"]
df["Efficiency P6 - ClockCounterWise"] = df["SpeedUp P6 - ClockCounterWise"]/df["Num. Nodes"]
df["Efficiency P9 - ClockCounterWise"] = df["SpeedUp P9 - ClockCounterWise"]/df["Num. Nodes"]

#   Calculate Efficiency - NotSequential
df["Efficiency P1 - NotSequential"] = df["SpeedUp P1 - NotSequential"]/df["Num. Nodes"]
df["Efficiency P4 - NotSequential"] = df["SpeedUp P4 - NotSequential"]/df["Num. Nodes"]
df["Efficiency P6 - NotSequential"] = df["SpeedUp P6 - NotSequential"]/df["Num. Nodes"]
df["Efficiency P9 - NotSequential"] = df["SpeedUp P9 - NotSequential"]/df["Num. Nodes"]



aggregate_operations = {
    'SpeedUp P1 - ClockCounterWise':'max',
    'SpeedUp P4 - ClockCounterWise':'max',
    'SpeedUp P6 - ClockCounterWise':'max',
    'SpeedUp P9 - ClockCounterWise':'max',
    'SpeedUp P1 - NotSequential':'max',
    'SpeedUp P4 - NotSequential':'max',
    'SpeedUp P6 - NotSequential':'max',
    'SpeedUp P9 - NotSequential':'max'
}
df = df.groupby(['Num. Nodes'], as_index=False).agg(aggregate_operations)
df["Ideal Speedup"] = df["Num. Nodes"]




fig = plt.figure(figsize=(8,8))
color='tab:blue'
#   Plotar os valores dos graficos para P1 e P4.
plt.plot(df['Num. Nodes'], df['SpeedUp P1 - ClockCounterWise'], marker='s', markersize=14, color='forestgreen', linewidth=5)
plt.plot(df['Num. Nodes'], df['SpeedUp P4 - ClockCounterWise'], marker='s', markersize=14, color='cyan', linewidth=5)
plt.plot(df['Num. Nodes'], df['SpeedUp P1 - NotSequential'], marker='.', markersize=14, color='orange', linewidth=2)
plt.plot(df['Num. Nodes'], df['SpeedUp P4 - NotSequential'], marker='.', markersize=14, color='darkblue', linewidth=2)
plt.plot(df['Num. Nodes'], df['Ideal Speedup'], marker='_', markersize=14, color='dimgrey', linewidth=2)
#   Define a escala do grafico
# plt.xscale('log', basex=2)
# plt.yscale('log', basey=2)
plt.xticks([1, 16, 32, 64, 128, 256, 512], [1, 16, 32, 64, 128, 256, 512], rotation=90)
plt.yticks([1, 16, 32, 64, 100, 128, 150, 200, 256, 300, 350, 400, 450, 512], [1, 16, 32, 64, 100, 128, 150, 200, 256, 300, 350, 400, 450, 512])

lines = [1, 16, 32, 64, 100, 128, 150, 200, 256, 300, 350, 400, 450, 512]
for i in lines:
    plt.axhline(i, linestyle='--', color='darkblue', linewidth='1.2')

#   Plota os labels
plt.tight_layout()
title = 'Projection Solver - 10000 Square - (P1, P4)'
subtitle = 'ClockCounterWise Vs NotSequential Algorithms - Speedup\n'
plt.suptitle(title, y=0.97, fontsize=20, fontweight='bold', ha='center', va='center')
plt.title(subtitle, x=0.48, fontsize=16, ha='center', va='center')
plt.subplots_adjust(top=0.90)
color='tab:blue'
plt.xlabel('Number of Procs - MPI', fontweight='bold')
plt.ylabel('Speedup', fontweight='bold', color=color)
#   Plota a legenda e o tamanho da figura
plt.legend(loc=(0.02, .69), frameon = True)
plt.grid()
plt.show()



fig2 = plt.figure(figsize=(8,8))
color='tab:blue'
#   Plotar os valores dos graficos para P6 e P9
plt.plot(df['Num. Nodes'], df['SpeedUp P6 - ClockCounterWise'], marker='s', markersize=14, color='gold', linewidth=5)
plt.plot(df['Num. Nodes'], df['SpeedUp P9 - ClockCounterWise'], marker='s', markersize=14, color='darkorange', linewidth=5)
plt.plot(df['Num. Nodes'], df['SpeedUp P6 - NotSequential'], marker='.', markersize=14, color='olive', linewidth=2)
plt.plot(df['Num. Nodes'], df['SpeedUp P9 - NotSequential'], marker='.', markersize=14, color='indigo', linewidth=2)
plt.plot(df['Num. Nodes'], df['Ideal Speedup'], marker='_', markersize=14, color='dimgrey', linewidth=2)
#   Define a escala do grafico
# plt.xscale('log', basex=2)
# plt.yscale('log', basey=2)
plt.xticks([1, 16, 32, 64, 128, 256, 512], [1, 16, 32, 64, 128, 256, 512], rotation=90)
plt.yticks([1, 16, 32, 64, 100, 128, 150, 200, 256, 300, 350, 400, 450, 512], [1, 16, 32, 64, 100, 128, 150, 200, 256, 300, 350, 400, 450, 512])

lines = [1, 16, 32, 64, 100, 128, 150, 200, 256, 300, 350, 400, 450, 512]
for i in lines:
    plt.axhline(i, linestyle='--', color='darkblue', linewidth='1.2')

#   Plota os labels
plt.tight_layout()
title = 'Projection Solver - 10000 Square - (P6, P9)'
subtitle = 'ClockCounterWise Vs NotSequential Algorithms - Speedup\n'
plt.suptitle(title, y=0.97, fontsize=20, fontweight='bold', ha='center', va='center')
plt.title(subtitle, x=0.48, fontsize=16, ha='center', va='center')
plt.subplots_adjust(top=0.90)
color='tab:blue'
plt.xlabel('Number of Procs - MPI', fontweight='bold')
plt.ylabel('Speedup', fontweight='bold', color=color)
#   Plota a legenda e o tamanho da figura
plt.legend(loc=(0.02, .69), frameon = True)
plt.grid()
plt.show()
