#!/usr/bin/python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math
import os
from pathlib import Path
from matplotlib.pyplot import figure, draw



#########################################################################
#   Configurações globais do grafico
plt.rcParams['axes.linewidth']=2
plt.rc('font', size=18)         # controls default text sizes
#########################################################################

#########################################################################
#   Configurações para salvar e plotar o grafico
BASE_PATH = os.getcwd()
PATH_SVG=BASE_PATH+"/../../../../inkscape/cap6/"
PATH_EPS=BASE_PATH+"/../../../../eps/cap6/"
fname = "projection_hexa10000_mpi"
fname_svg = PATH_SVG+fname+".svg"
fname_eps = PATH_EPS+fname+".eps"
PLOT_FIG = True
SAVE_FIG = False
#########################################################################



#   Leitura do csv para pandas
##### Resultado para CentralDifferenceLinear Local Method
# df = pd.read_csv('./data/cdl-linear_mpi-openmp.csv')
##### Resultado para CentralDifferenceLinear Local Method - Multiplicate Operations
df = pd.read_csv('./data/projection_10000_hexa_mpi.csv', sep=";")
pd.set_option('expand_frame_repr', False)

#   Calculate SpeedUp
df["SpeedUp P1 - Overlapping"] = df.iloc[0]["P1 - Runtime (s)"]/df["P1 - Runtime (s)"]
df["SpeedUp P3 - Overlapping"] = df.iloc[0]["P3 - Runtime (s)"]/df["P3 - Runtime (s)"]
df["SpeedUp P6 - Overlapping"] = df.iloc[0]["P6 - Runtime (s)"]/df["P6 - Runtime (s)"]
df["SpeedUp P9 - Overlapping"] = df.iloc[0]["P9 - Runtime (s)"]/df["P9 - Runtime (s)"]

#   Calculate Efficiency
df["Efficiency P1 - Overlapping"] = df["SpeedUp P1 - Overlapping"]/df["Num. Nodes"]
df["Efficiency P3 - Overlapping"] = df["SpeedUp P3 - Overlapping"]/df["Num. Nodes"]
df["Efficiency P6 - Overlapping"] = df["SpeedUp P6 - Overlapping"]/df["Num. Nodes"]
df["Efficiency P9 - Overlapping"] = df["SpeedUp P9 - Overlapping"]/df["Num. Nodes"]



#   Aggregate efficiency
aggregate_operations = {
    'Efficiency P1 - Overlapping':'max',
    'Efficiency P3 - Overlapping':'max',
    'Efficiency P6 - Overlapping':'max',
    'Efficiency P9 - Overlapping':'max'
}
df = df.groupby(['Num. Nodes'], as_index=False).agg(aggregate_operations)



fig = plt.figure(figsize=(8,8))
color='tab:blue'
#   Plotar os valores dos graficos
plt.plot(df['Num. Nodes'], df['Efficiency P1 - Overlapping'], marker='s', markersize=14, color='y', linewidth=2)
plt.plot(df['Num. Nodes'], df['Efficiency P3 - Overlapping'], marker='.', markersize=14, color='c', linewidth=2)
plt.plot(df['Num. Nodes'], df['Efficiency P6 - Overlapping'], marker='x', markersize=14, color='b', linewidth=2)
plt.plot(df['Num. Nodes'], df['Efficiency P9 - Overlapping'], marker='o', markersize=14, color='g', linewidth=2)
#   Define a escala do grafico
plt.xscale('log', basex=2)
# plt.yscale('log', basey=2)
plt.xticks([1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024], [1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024], rotation=45)
plt.yticks([0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1], [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1])

lines = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
for i in lines:
    plt.axhline(i, linestyle='-', color='darkblue', linewidth='1.2')


#   Plota os labels
plt.tight_layout()
title = 'Projection Solver - 10000 Hexahedrons'
subtitle = 'Overlapping Algorithm - Efficiency\n'
plt.suptitle(title, y=0.97, fontsize=20, fontweight='bold', ha='center', va='center')
plt.title(subtitle, x=0.48, fontsize=16, ha='center', va='center')
plt.subplots_adjust(top=0.90)
color='tab:blue'
plt.xlabel('Number of Procs - MPI', fontweight='bold')
plt.ylabel('Speedup', fontweight='bold', color=color)
#   Plota a legenda e o tamanho da figura
plt.legend(loc=(0.37, .73), frameon = True)
plt.grid()
plt.show()
