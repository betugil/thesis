#!/usr/bin/python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math
import os
from pathlib import Path
from matplotlib.pyplot import figure, draw


df = pd.read_csv("data/crankshaft_5mm_17810_mpi-vs-openmp_by-core.csv", sep=";")
