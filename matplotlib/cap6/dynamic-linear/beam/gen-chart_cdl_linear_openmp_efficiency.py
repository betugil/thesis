#!/usr/bin/python3
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import math
import os
from pathlib import Path
from matplotlib.pyplot import figure, draw



#########################################################################
#   Configurações globais do grafico
plt.rcParams['axes.linewidth']=2
plt.rc('font', size=18)         # controls default text sizes
#########################################################################

#########################################################################
#   Configurações para salvar e plotar o grafico
BASE_PATH = os.getcwd()
PATH_SVG=BASE_PATH+"/../../inkscape/cap6/"
PATH_EPS=BASE_PATH+"/../../eps/cap6/"
fname = "cdl_linear_multiplicate_openmp_efficiency"
fname_svg = PATH_SVG+fname+".svg"
fname_eps = PATH_EPS+fname+".eps"
PLOT_FIG = True
SAVE_FIG = True
#########################################################################


#   Leitura do csv para pandas
df = pd.read_csv('./data/cdl-linear_multiplicate_openmp_efficiency.csv')
print(df)

#   Aloca figura para permitir alteracao
fig, ax = plt.subplots()

#   Plota os labels
plt.tight_layout()
title = 'Central Difference Linear Local'
subtitle = 'Multiplicate Methods with OpenMP\n'
# plt.suptitle(title, y=0.97, fontsize=20, fontweight='bold', ha='center', va='center')
# plt.title(subtitle, x=0.48, y=0.99, fontsize=16, ha='center', va='center')
# plt.subplots_adjust(top=0.90)
plt.xlabel('Number of Threads', fontweight='bold')
plt.ylabel('Efficiency', fontweight='bold')

#   Define a escala do grafico
plt.xscale('log', basex=2)

#   Plotar os valores dos graficos
plt.plot(df['Number Ranks'], df['31104 DOFs'], marker='o', markersize=14, color='r')
plt.plot(df['Number Ranks'], df['221952 DOFs'], marker='o', markersize=14, color='y')
plt.plot(df['Number Ranks'], df['1672704 DOFs'], marker='o',  markersize=14, color='g')

#   Configura os labels em 'x' e 'y'
# x = [str(i) for i in x]
n=32
values_multiples_2 = [ 2**i for i in range(0,int(math.log(n,2))+1) ]
str_values_multiples_2 = [ str(i) for i in values_multiples_2 ]
plt.xticks(values_multiples_2, str_values_multiples_2)
values_y = np.arange(0.1,1.1,0.1)
str_values_y = [ str(round(i,1)) for i in values_y ]
plt.yticks(values_y, str_values_y)

#   Define as linhas do grid
ax.xaxis.grid(True, linestyle='--', color='k', linewidth='0.5')
lines = np.arange(0.1,1.1,0.1)
for i in lines:
    ax.axhline(i, linestyle='--', color='k', linewidth='0.5') # horizontal lines

#   Plota a legenda e o tamanho da figura
# plt.legend()
ax.legend(['$31\,104$ DOFs', '$221\,952$ DOFs', '$1\,672\,704$ DOFs'])
fig.set_size_inches(8, 8)

#   Exibe a figura
if (PLOT_FIG):
    plt.show()

#   Salva a figura
if (SAVE_FIG):
    fn = Path(fname_svg).expanduser()
    draw() # necessary to render figure before saving
    fig.savefig(fn, bbox_inches='tight')

    fn = Path(fname_eps).expanduser()
    draw() # necessary to render figure before saving
    fig.savefig(fn, bbox_inches='tight')
