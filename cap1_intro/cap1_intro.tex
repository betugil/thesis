%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Introduction}\label{intro}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Computer simulations use numerical models to analyze physical problems, allowing the modification of 
parameters and initial conditions to provide relevant information about their behavior. In this 
context, the finite element method (FEM) is a numerical procedure that obtains approximate solutions 
of boundary value problems (BVPs). This method calculates the approximate solution of BVPs in the 
whole domain through a discretization into sub-domains called finite elements. The high-order FEM 
(HO-FEM), also called spectral $hp$-FEM, obtains a converged approximate solution by mesh refinement 
and increasing the polynomial order of the interpolation functions 
\cite{karniadakis2005,ML_Bittencourt2014}. The development of finite element software architectures 
that support several applications and are generic, flexible, and reusable has been considered in the 
literature \cite{J_Mackerle2000,bangerth2007,kawabata2009,anzt2010}. The HO-FEM makes significant 
demands on memory and processing resources, mainly in complex applications where high-performance 
computing is required.

Many commercial software implementing the FEM have been developed in the last six decades, for 
instance Ansys, Nastran, Adina, LS-Dyna, Abaqus, HyperMesh, COMSOL, among others. Most of them 
implements the $h$-version of the FEM, where the approximate solution is improved by refining the 
size of elements, for many different problems including structures, electromagnetism, fluid 
mechanics, acoustics and many others. The $p$- and $hp$-versions of the FEM, later called HO-FEM, 
were created at the end of the 1970 decade and the main feature is the exponential convergence rate 
of approximations for smooth solutions. One of the main historical difficulties to the dissemination 
of these versions was the lack of commercial and open-source software. StressCheck was one of the 
first finite element analysis software based on the $p$-FEM with focus on solid mechanics. However, 
it was inefficient for the analysis of practical problems \cite{ESRD_2009}.

Analogously, many open source software for the FEM have been developed such as CalculiX, deal.II, 
Dune, Elmer, FEniCS, FreeFEM, GetFEM++, Hermes, Libmesh, MOOSE, RANGE, Z88, PZ, among others. Many 
of these software packages implement the $p$- and $hp$-versions of the FEM and are based on the 
object-oriented paradigm using the C++ and Python languages.

Nowadays, there is a large community working on high-order numerical methods, see references herein 
\cite{karniadakis2005, ML_Bittencourt2014}. Along the last two decades, many improvements were 
developed including basis functions, efficient numerical integration and differential quadrature, 
efficient solvers and pre-conditioners, mesh generation, visualization of results and 
applications in many different problems. 


% The \hpfem software was based on previous versions of the ACDPOOP and SAT software implemented at the 
% National Laboratory for Scientific Computing (LNCC) and by the group at UNICAMP, where this thesis 
% was developed. These software used moderate polynomial orders applied to structural analysis. The 
% \hpfem software was designed to have arbitrary polynomial orders, implement the $h$, $p$ and $hp$ 
% versions of the FEM, use of nodal and modal bases, efficient numerical integration procedures, 
% efficient calculation of element operators, hybrid parallelism and application to different problems. 
% In addition, the development of the local solvers based on the least squares method implemented in 
% hybrid architectures make the software appropriate to be used in the next generation of hexaflop 
% clusters. 
    
%O software \hpfem foi desenvolvido baseado em versões anteriores dos softwares ACDPOOP e SAT 
%\cite{ACS_Guimaraes1989a,ML_Bittencourt1999a}.
%implementados no Laboratório Nacional de Computação Científica (LNCC) e pelo grupo da UNICAMP onde esta 
%tese foi desenvolvida. No grupo do Laboratório de Simulação Computacional do Departamento de Sistema 
%Integrados (DSI) da UNICAMP foi desenvolvido trabalhos com a arquitetura de software do MEF-AO em MATLAB 
%aplicadas à análise estrutural ~\cite{AC_Nogueira2002, 
%M_Vazquez2004, T_Vazquez2008, M_Miano2009, FF_Bargos2009, CFR_Santos2011, FAC_Furlan2011, 
%RA_Augusto2012}. Nestes trabalhos, a arquitetura em MATLAB foi projetada para ter ordens polinomiais 
%arbitrárias, uso de bases nodais e modais, 
%procedimentos de integração numérica, cálculo eficiente de operadores dos elementos.

%No trabalho ~\citet{GL_Valente2012}, a arquitetura em MATLAB do \hpfem foi ampliada e portada 
%para a linguagem C++. O trabalho desta tese consistiu na implementação do paralelismo híbrido aplicado a 
%diferentes problemas. Além disso, os algoritmos locais de solução com base no método dos mínimos 
%quadrados e a possibilidade do uso de malhas com distribuição não-uniforme de ordem polinomial 
%em arquiteturas híbridas torna o software apropriado para a próxima geração 
%de clusters hexaflop. 

The \hpfem software was developed based on previous versions of the ACDPOOP and SAT software
\cite{ACS_Guimaraes1989a,ML_Bittencourt1999a} implemented at the National Laboratory for Scientific 
Computing (LNCC) and by the UNICAMP group where this thesis was developed. This group 
developed also the \hpfem MATLAB version  applied mainly to structural analysis 
\cite{AC_Nogueira2002,M_Vazquez2004, T_Vazquez2008, M_Miano2009, FF_Bargos2009, CFR_Santos2011, FAC_Furlan2011,
RA_Augusto2012}. The MATLAB architecture was designed taking into account arbitrary polynomial 
orders, use of nodal and modal bases, numerical integration procedures and efficient calculation 
of element operators.

In ~\citet{GL_Valente2012}, the \hpfem MATLAB architecture was expanded and ported to the C++ 
language. The work of this thesis consisted of the hybrid parallelism implementation applied to 
different problems. The local solution algorithms based on the least squares method and the 
possibility of using meshes with non-uniform polynomial distribution on hybrid architectures make 
the software suitable for the next generation of exaflops computers \cite{SPPEXA2020}.

%==================================================================================================
%	New part histoty about finite element softwares
\section{Literature Review and Related Finite Element Software}\label{intro:fem_packages}

%	General descriptions
%Most of the finite element packages are proprietary software, but we will discuss the main 
%open-source software tools. The majority of these packages are object-oriented and modular. They are 
%applied principally in the fluid dynamic area.

\citet{bangerth2007} proposed a modular architecture and a partitioned library in C++ for the FEM 
without loss of performance. The goal was to organize the code in independent modules that can be 
arbitrarily used. In specific cases, preprocessor directives and constants are used to construct 
finite element meshes, avoiding the use of virtual methods and reducing the system overhead.

\citet{cantwell2011} proposed to encapsulate the spectral/$hp$ method in the Nektar++ 
software for fluid dynamics applications \cite{necktar:2013:online}. The best combinations of $h$ 
(mesh size) and $p$ (polynomial order) refinements were studied for the Helmholtz problem.

In \citet{kawabata2009, fu2008}, authors used the FEM in parallel cluster environments and 
identified the bottlenecks of running the parallel software. The work in \citet{kawabata2009} 
identified the parts of code with the highest memory demand and time consumption, and then implemented 
parallel processing in these pieces of code. In \citet{fu2008}, three different meshes were considered 
for a two-dimensional plane strain model of a dam and its foundations. Two parameters were changed: 
the interpolation order for each mesh and the number of processors. The author concluded that the 
increase in degrees of freedom enabled better performance. However, the increased number of 
processors improved the performance up to a certain limit depending on the communication and 
synchronization overheads.

In terms of parallel strategies for finite difference schemes, a space-filling curve algorithm was 
used to solve workload imbalance of static partition of the domain in \cite{C_Wang2015, 
K_Wang2015}. The development of an adaptive mesh refinement procedure based on finite differences was 
proposed in \cite{C_Wang2015}. In \citet{K_Wang2015}, ParMetis is used for grid partitioning of a 
black oil discrete model. Other works involve different numerical methods such as \cite{R_Oguic2015}, 
where the 3D Navier-Stokes equations based on fourth-order compact schemes are solved, with hybrid 
OpenMP/MPI parallelization, presenting attractive scalability when using up to four MPI tasks. A 
different parallel strategy was used by \cite{J_Chan2016}, focusing on high order discontinuous 
Galerkin methods using a single GPU with an unified approach to multi-threading programming model.

%	deal.ii
The deal.ii package is a C++ library with a Lesser General Public License (LGPL) \cite{bangerth2007}. 
The unified interface of deal.ii can handle problems in one, two, or three dimensions and 
enables adaptive mesh refinement through error estimators and local indicators. 
This FEM package works by adapting $h$, $p$, and $hp$ for continuous and discontinuous 
Lagrange, Nedelec, and Raviart-Thomas elements for any polynomial order. deal.ii also implements 
parallelism, with scalable simulations of up to $16\,000$ processors using multithreading and 
Message Passing Interface (MPI) ranks.

%	Libmesh
The Libmesh package was created with the goal of supporting adaptive $h$-refinement. Nowadays, it 
also works with finite element and finite volume simulations for $p$ and $hp$ refinements for some 
element types \cite{kirk2006}. The libraries offer procedures that allow developers to perform a few 
calls of the principal functions, rather than many calls to smaller functions, thus avoiding the 
overhead caused by virtual function calls of abstract base classes. Code debugging for smaller 
problems in 2D can be applied immediately to large problems. Libmesh uses algorithms from METIS and 
PARMETIS to partition weighted graphs in serial and parallel for 1D, 2D, and 3D meshes. The software 
uses other external tools as iterative solvers and preconditioners in serial applications such as 
LASPack and parallel applications using PETSc.

%	z88
The z88 freeware software project operates on Linux, Windows, and Mac OS X \cite{z88_2014}. This FEM 
package can solve non linear problems with large displacements, linear static analysis, thermal and 
thermomechanical problems, and natural frequency problems. This software has three kinds of solvers: 
a direct Cholesky solver with Jennings storage, an iterative solver for sparse matrices with conjugate
gradient (CG) preconditioners and a direct solver with sparse storage and 
multi-core processing.

%	DUNE
In addition to the FEM, the Distributed and Unified Numerics Environment (DUNE) supports finite volume 
and finite difference methods \cite{blatt2008}. DUNE is offered with a GPL 2 license with runtime 
exception, which allows its use in proprietary software. Among the available linear algebra methods, 
DUNE uses BLAS1 (basic linear algebra subroutine) functions, stationary iterative methods, and 
parallel preconditioners for the multigrid method. The software also supports parallelization with 
$h$ and $p$ refinements. The implemented solvers can handle linear and non linear problems using time 
discretization methods, e.g., Runge--Kutta and multistep methods. DUNE uses static polymorphism, 
allowing the compiler to apply additional optimization, e.g., inline functions. The software is 
modulated to allow each package to be tested independently.

%	Nektar++
Nektar++ is a finite element package that allows operations with low ($h$-version) 
and high ($p$-version) polynomial orders \cite{cantwell2015}. Nektar++ is designed to operate with 
meshes of hybrid elements, i.e., prisms and hexahedrons, triangles, quadrilaterals, and tetrahedra. In 
addition, it is possible to define domains with segments, planes, volumes, curves, and surfaces. The 
software works with continuous and discontinuous Galerkin operators. Nektar++ has a parallel version 
which had strong scalability up to 2048 cores.

%	Hermes.
The Hermes software was developed as a fast development of FEM solvers 
\cite{solin2014}. It implements $hp$ adaptivity, but only works on 2D 
applications and for polynomial orders of up to 10. Furthermore, Hermes runs 
with multithreading parallelization using OpenMP. This package has its own 
module for data visualization using OpenGL and VTK to output meshes and 
solutions.

%	GetFEM++
The library GetFEM++ solves linear and nonlinear systems of partial differential equations 
\cite{renard2014}. The library is written in C++ with interfaces for Python, Matlab, and Scilab, 
which collaborate with GetFEM++ for post-processing operations. Among the main features, the library 
was designed with variables, data, and terms for the solution of classic models such as the Helmholtz 
problem with Dirichlet, contact and Neumann boundary conditions and elasticity in small and large 
deformations. There is also a module for generating regular meshes or importing meshes with GID, 
GMSH, and EMC2 formats.

%	Moose
The Multiphysics Object-Oriented Simulation Environment (MOOSE) is a finite element framework 
\cite{gaston2009} with high-level interfaces and simple APIs that facilitate the implementation of 
real-world problems. MOOSE performs $h$, $p$, and $r$ refinements. It is possible to build the domain 
geometry, generate meshes, and visualize the solution process and final results. The FEM module and 
mesh adaptivity are accessed through the libMesh library.

% The PZ environment implements one and two dimensional finite elements with arbitrary interpolation 
% orders and applicable to a variety of systems of differential equations \cite{BERNARDDEVLOO1997133}. 

%  New described about PZ
The object-oriented framework PZ implements one-, two- and three-dimensional finite elements with 
arbitrary interpolation orders, various solvers as Krylov space, direct and iterative methods. The 
PZ makes a substantial distinction between the generation of approximation spaces, the geometric 
modeling, and the definition of the variational statement \cite{BERNARDDEVLOO1997133, 
PRB_Devloo2002}.


Unlike the packages mentioned above, the purpose of this work was the implementation of the HO-FEM 
software with local solvers and hybrid parallelism. In addition, the
software allow, using local solvers, the simpler use of non-uniform interpolation in the elements.


%==================================================================================================
% Problem Statement and Research Objectives
\section{Research Objectives and Contributions}\label{intro:contributions}

The main objective of this thesis was to develop an open source software for the HO-FEM using local 
algorithms, based on least squares, for transient linear and non-linear structural problems with 
explicit temporal integration. The system of equation are solved for each element and the global 
solutions are obtained by the weighted average of the local solutions at the element interfaces. 
These local algorithms were implemented for parallel execution in clusters of heterogeneous machines 
with multiple nodes and cores. The \hpfem package was written in C ++ and is available at 
\href{www.hp2fem.org}{www.hp2fem.org}. It has been tunned for the IBM Power and Intel-X86 
architectures, allowing testing and profiling of the sub-packages independently.

%   Significant contributions
The main contributions of this work are as follows:
\begin{itemize}
%   The study of the differente parallel version.
\item The development of a software architecture for the HO-FEM and its parallel implementation on
hybrid computers using the libraries MPI and OpenMP, considering the possibility of non-uniform 
polynomial distribution ($p$-non-uniform) for the elements. 

%   Threatment of deadlock use
\item A topology mapping for the sub-domain meshes avoiding deadlock among processors during the exchange 
of messages. The finite element coordinates are used to construct the mapping of the local 
degrees of freedom stored in each partition to send and receive solutions among neighbor sub-domain meshes.

%   Scalable local solver
\item Scalable hybrid parallel implementation of linear and non-linear transient solvers
using linear algebra packages such as BLAS and LAPACK. 
The high order data are generated locally in each sub-domain  
reducing the memory usage. 
\end{itemize}

We presented the scientific works in the following conferences:
\begin{itemize}
    \item J.L. Suzuki, G.L. Valente, C.F. Rodrigues, M.L. Bittencourt, Application of high-order 
    minimum energy bases for transient non-linear structural problems. MECSOL-2017, Joinville, 2017.
    \item G.L. Valente, E. Borin. and M.L. Bittencourt. Optimization of the high performance soft- 
ware \hpfem: a C++ framework for the high-order finite element method. ICOSAHOM2016 - International 
Conference on Spectral and High Order Methods, Rio de Janeiro, RJ, 2016.
    \item G.L. Valente, M.L. Bittencourt and E. Borin. High order finite element method on the IBM 
power systems. High performance computing applied to structural mechanics. 5th European Conference on 
Computational Mechanics (ECCM V), Barcelona, Espanha, 2014.
    \item G.L. Valente, M.L. Bittencourt and E. Borin. Perfilamento e otimização do \hpfem no IBM 
Blue Gene/P. IV Escola Regional de Alto Desempenho de São Paulo. São Carlos, SP, 2013.
\end{itemize}

The following papers have been submitted to journals:

\begin{itemize}
    \item G.L. Valente, M.L. Bittencourt and E. Borin. \hpfem - A p-non-uniform software 
architecture for the high-order finite element method on massively parallel computers. 
    \item A.P.C. Dias, J.L. Suzuki, G.L. Valente and M.L. Bittencourt,. Minimum energy high-order bases 
applied to non-linear structural and contact problems.
\end{itemize}


%==================================================================================================
%   Thesis outline
\section{Layout of the thesis}\label{intro:outline}

%   P2 - Describing of finite element metlhod
The thesis is presented in seven chapters. Concepts of the HO-FEM important to this work are 
described in Chapter~\ref{ho_fem}: the tensor product procedure to calculate the element operators 
which were fundamental to accelerate \hpfem reducing the memory usage; the local solvers of the 
\hpfem; the $p$-non-uniform approach applied to the projection problem; and the central difference 
local solvers applied to linear and non-linear problems.

%   P3 - Describing the concepts of HPC.
Chapter~\ref{hpc} presents the primary concepts of high-performance computing (HPC) 
used in this thesis. In summary, the characteristics of a parallel system using distributed and 
shared memory, the interfaces and/or libraries employed to implement the parallel version of \hpfem 
and the main scalability measures are described.

%   P4 - Describes the serial version
The framework architecture is presented in Chapter~\ref{hp2fem_serial} with the low-coupling hierarchy of  
classes implemented with C++ object-oriented programming. This chapter also 
presents a short description of the framework to implement the HO-FEM package. Subsequently, the key 
features of the \hpfem considered as contributions are also discussed. We also 
describe the parts of \hpfem and the C++ classes which were remodeled to accelerate the serial version; for 
instance, adapting some functions to use external optimized linear algebra libraries.

%   P5 - Describes the parallel version.
The parallel version of the \hpfem is discussed in Chapter~\ref{hp2fem_parallel}. The implementation 
of partitioning of the global finite element meshes and the methodology to distribute them among 
processors are described. Besides, four different cases to distribute and solve a global finite 
element model are presented. At last, a parallel version of the central difference local method 
using hybrid parallelism with OpenMP and MPI is discussed.

%   P6 - Shows the results of the thesis.
Results are presented in Chapter~\ref{results} considering the evolution of the software 
acceleration of the serial version to the scalability of the parallel version. We give the 
configurations of the supercomputers used at the Argonne National Laboratory of the U.S Department of 
Energy and the cluster of Intel system provided by the Center for Computational Engineering \& Sciences 
located at the Institute of Chemistry of Unicamp. We present the software validation for 
$p$-non-uniform meshes to projection problem as the scalability cases to the same solver. 
For time integration solvers, we use the central difference local method to evaluate the 
scalability of the parallel version of the \hpfem. The final considerations and future works are then 
presented in Chapter~\ref{conclusions}.


% This article is organized as follows. Section~\ref{mef} introduces the solution 
% strategies used in the HO-FEM software. Section~\ref{architecture} describes the 
% main characteristics of the $(hp)^2$ FEM packages. 
% Section~\ref{optimizations_and_parallel} discuss the source code profiling and 
% updates made to improve the performance of the package. We also presents the 
% strategies implemented to construct the parallel model. To validate the proposed 
% architecture, we derive solutions for projection problems in 
% Section~\ref{results} using the serial model. Section~\ref{results} also 
% presents optimization and memory reduction results for the serial code and 
% examines the scalability of the \hpfem parallel version. 
