rm $(find . -name *.aux)
rm $(find . -name *~)
rm *.aux *.lof *.log *.lot *.nlo *.out *.toc *.pdf *.bbl *.blg *.snm *.nav
