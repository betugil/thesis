\beamer@sectionintoc {1}{Introduction and Motivation}{3}{0}{1}
\beamer@sectionintoc {2}{Thesis Contributions}{9}{0}{2}
\beamer@sectionintoc {3}{Goals}{10}{0}{3}
\beamer@sectionintoc {4}{Methodology}{11}{0}{4}
\beamer@subsectionintoc {4}{1}{HO-FEM}{11}{0}{4}
\beamer@subsectionintoc {4}{2}{{$(hp)^2$FEM}}{13}{0}{4}
\beamer@subsectionintoc {4}{3}{High Performance Computing tools}{18}{0}{4}
\beamer@sectionintoc {5}{Parallel {$(hp)^2$FEM}\ Version}{24}{0}{5}
\beamer@sectionintoc {6}{Results}{33}{0}{6}
\beamer@sectionintoc {7}{Conclusions}{65}{0}{7}
